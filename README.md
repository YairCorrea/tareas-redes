## Tareas - Redes de Computadoras - 2020-2

![Pipeline Status](https://gitlab.com/Redes-Ciencias-UNAM/2020-2/tareas-redes/badges/master/pipeline.svg)

En este repositorio estaremos manejando las tareas de la materia de [redes de computadoras][gitlab-redes] que se imparte en la [Facultad de Ciencias, UNAM][fciencias-redes] en el semestre 2020-2.

### Estructura de directorios

+ Cada alumno deberá tener una carpeta dentro de la que hará entrega de sus tareas y prácticas
+ Si la práctica es en equipo, uno sube la carpeta de la práctica y los demás integrantes entregan una *liga simbólica* a la carpeta correspondiente de la práctica.
+ Cada tarea o práctica deberá tener lo siguiente:
    * Un archivo `README.md` donde se entregará la documentación en formato _markdown_. Se puede hacer uso de un directorio `img/` para guardar las imágenes o capturas de pantalla necesarias para la documentación
    * Un archivo `Makefile` que servirá para compilar el programa y hacer pruebas

```
content/entregas/AndresHernandez/
├── .gitkeep
├── README.md
├── tareas
│   ├── tarea-0/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   └── ...
│   ├── tarea-1/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   ├── Makefile
│   │   ├── programa.c
│   │   └── ...
│   ├── ...
│   └── tarea-n/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   └── ...
└── practicas/
    ├── practica-1/
    │   ├── img/
    │   │   └── ...
    │   ├── README.md
    │   └── ...
    ├── practica-2/ -> ../../JuanCamacho/practicas/practica-2/
    ├── ...
    └── practica-m/
        ├── img/
        │   └── ...
        ├── README.md
        └── ...
```

--------------------------------------------------------------------------------

##### Vista en GitLab

+ <https://gitlab.com/Redes-Ciencias-UNAM/2020-2/tareas-redes>

##### Vista web

+ <https://Redes-Ciencias-UNAM.gitlab.io/2020-2/tareas-redes/>

--------------------------------------------------------------------------------

[gitlab-redes]: https://SistemasOperativos-Ciencias-UNAM.gitlab.io/ "Página en GitLab"
[fciencias-redes]: "http://www.fciencias.unam.mx/docencia/horarios/20202/1556/714" "Redes de Computadoras - 2020-2"

